package mod3;

import java.util.Scanner;

public class Ejer_4 {

	public static void main(String[] args) {
		
		System.out.println("Indic� tu categor�a: ");
		char categor�a = extracted().next().charAt(0);
		
		if (categor�a=='a'){System.out.println("Tu categor�a \'"+categor�a+"\' es de hijo");}
		else if(categor�a=='b') {System.out.println("Tu categor�a \'"+categor�a+"\' es de padre");}
		else if(categor�a=='c') {System.out.println("Tu categor�a \'"+categor�a+"\' es de abuelo");}
		else {System.out.println("Tu categor�a \'"+categor�a+"\' no existe");}
	}
	
	public static Scanner extracted(){
		
		return new Scanner(System.in);
	}

}
