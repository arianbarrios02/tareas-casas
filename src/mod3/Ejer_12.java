package mod3;

import java.util.Scanner;

public class Ejer_12 {

	public static void main(String[] args) {
		
		System.out.println("Ingres� un n�mero entre 1 y 36 para decidir en cual docena entra: ");
		int docena = extracted().nextInt();
		
		if(docena>=1 && docena<=12) {System.out.println("Tu n�mero est� dentro de la primer docena");}
		else if(docena>=13 && docena<=24) {System.out.println("Tu n�mero est� dentro de la segunda docena");}
		else if(docena>=25 && docena<=36) {System.out.println("Tu n�meroe st� dentro de la tercer docena");}
		else {System.out.println("Tu n�mero est� fuera del rango indicado");}
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
}
