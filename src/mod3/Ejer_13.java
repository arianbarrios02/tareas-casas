package mod3;

import java.util.Scanner;

public class Ejer_13 {

	public static void main(String[] args) {
		
		System.out.println("Ingres� un mes (todo con min�scula): ");
		String mes = extracted().nextLine();
		
		switch(mes){
		case "abril": case "junio": case "septiembre": case "noviembre":
			System.out.println("Ese mes tiene 30 d�as");
			break;
		case "febrero":
			System.out.println("Ese mes tiene 28 d�as");
			break;
		case "enero": case "marzo": case "mayo": case "julio": case "agosto": case "octubre": case "diciembre":
			System.out.println("Ese mes tiene 31 d�as");
			break;
		default:
			System.out.println("Pusiste un mes incorrecto o escrito con may�sculas");
		}		
		
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
}
