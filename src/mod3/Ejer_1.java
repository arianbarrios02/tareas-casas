package mod3;

import java.util.Scanner;

public class Ejer_1 {

	public static void main(String[] args) {
		
		float nota1, nota2, nota3, promedio;
		
		System.out.print("Ingrese la primer nota: ");
		nota1 = extracted().nextFloat();
		System.out.print("Ingrese la segunda nota: ");
		nota2 = extracted().nextFloat();
		System.out.print("Ingrese la tercer nota: ");
		nota3 = extracted().nextFloat();
		
		promedio = (nota1+nota2+nota3)/3;
		
		if(promedio>=7) {
			System.out.println("Con tu promedio "+promedio+", has aprobado.");
		}
		else {
			System.out.println("Con tu promedio "+promedio+", has desaprobado.");
		}
		
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}

}
