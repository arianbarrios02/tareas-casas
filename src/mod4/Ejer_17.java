package mod4;

import java.util.Scanner;

public class Ejer_17 {

	public static void main(String[] args) {
		
		System.out.println("Ingres� un n�mero y te aparecer� su tabla: ");
		int tabla = extracted().nextInt();
		
		int contador=0;
		int suma=0;
		
		while (contador<11) {
			System.out.println(tabla+"x"+contador+"="+(tabla*contador));
			suma = suma+tabla*contador*(1-tabla*contador%2);
			contador++;
		}
		System.out.println("Se sumaron todos los resultados pares, d�ndo como resultado: "+suma);
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
}
