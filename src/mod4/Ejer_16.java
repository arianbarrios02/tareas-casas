package mod4;

import java.util.Scanner;

public class Ejer_16 {

	public static void main(String[] args) {
		
		System.out.println("Ingres� un n�mero y te aparecer� su tabla: ");
		int tabla = extracted().nextInt();
		
		for(int i=1; i<=10; i++) {
			int resultado = tabla*i;
			System.out.println(tabla+"x"+i+"="+resultado);
		}
		
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
}
