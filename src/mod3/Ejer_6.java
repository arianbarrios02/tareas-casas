package mod3;

import java.util.Scanner;

public class Ejer_6 {

	public static void main(String[] args) {
		
		System.out.println("Ingres� hace cuantos a�os estudi�s: ");
		int a�os = extracted().nextInt();
		
		if(a�os>=0) {
			if(a�os==0) {System.out.println("Est�s en el jard�n de infantes");}
			else if(a�os<=6) {System.out.println("Est�s en la primaria");}
			else if(a�os>6) {
				if(a�os<=12) {System.out.println("Est�s en la secundaria");}
				else{System.out.println("Est�s fuera de rango");}
			}
		}
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}

}
