package mod3;

import java.util.Scanner;

public class Ejer_9 {

	public static void main(String[] args) {
		
		System.out.println("Piedra Papel o Tijera");
		System.out.print("(Jugador 1) Ingres� tu elecci�n: ");
		int jugador1 = extracted().nextInt();
		System.out.print("(Jugador 2) Ingres� tu elecci�n: ");
		int jugador2 = extracted().nextInt();
		
		if(jugador1==1 && jugador2==1) {System.out.println("Ambos empatan");}
		else if(jugador1==2 && jugador2==2) {System.out.println("Ambos empatan");}
		else if(jugador1==3 && jugador2==3) {System.out.println("Ambos empatan");}
		else if(jugador1==3 && jugador2==2) {System.out.println("El jugador 1 gana");}
		else if(jugador1==1 && jugador2==3) {System.out.println("El jugador 1 gana");}
		else if(jugador1==2 && jugador2==1) {System.out.println("El jugador 1 gana");}
		else if(jugador1==1 && jugador2==2) {System.out.println("El jugador 2 gana");}
		else if(jugador1==2 && jugador2==3) {System.out.println("El jugador 2 gana");}
		else if(jugador1==3 && jugador2==1) {System.out.println("El jugador 2 gana");}
		
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
	
}
