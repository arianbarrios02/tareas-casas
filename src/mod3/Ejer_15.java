package mod3;

import java.util.Scanner;

public class Ejer_15 {

	public static void main(String[] args) {
		
		System.out.print("Ingres� la categor�a de tu auto: ");
		char categor�a = extracted().next().charAt(0);
		
		switch(categor�a) {
		case 'a': System.out.println("Tu auto tiene: \nCuatro ruedas\nUn motor");
			break;
		case 'b': System.out.println("Tu auto tiene: \nCuatro ruedas\nUn motor\nAire acondicionado\nCerradura centralizada");
			break;
		case 'c': System.out.println("Tu auto tiene: \nCuatro ruedas\nUn motor\nAire acondicionado\nCerradura centralizada\nAirbag");
			break;
		default:
			System.out.println("La categor�a que ingresaste no est� listada");
			break;
		}
		
	}
	
	private static Scanner extracted() {
		return new Scanner(System.in);
	}
}
